<?php

namespace Drupal\event_share\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class EmailFormConstraintValidator extends ConstraintValidator{

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
      foreach ($items as $item){

        $this->context->addViolation($constraint->notValidEmail, ['%value' => $item->value]);

      }
  }

}