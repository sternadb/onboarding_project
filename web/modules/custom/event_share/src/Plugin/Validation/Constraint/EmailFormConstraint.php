<?php

namespace Drupal\event_share\Plugin\Validation\Constraint;

use Composer\Semver\Constraint\Constraint;
use Symfony\Component\Validator\Constraints\Email;

/**
 * Count constraint.
 *
 * Overrides the symfony constraint to use the strict setting.
 *
 * @Constraint(
 *   id = "Email",
 *   label = @Translation("Email", context = "Validation")
 * )
 */
class EmailFormConstraint extends Constraint {

  /**public $strict = TRUE;


   * {@inheritdoc}

  public function validatedBy() {
    return '\Symfony\Component\Validator\Constraints\EmailValidator';
  }
   * */

  public $notValidEmail = "Enter a valid email address";

}