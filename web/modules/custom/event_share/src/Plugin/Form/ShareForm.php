<?php
/**
 * @file
 * Contains \Drupal\event_share\Plugin\Form.
 */
namespace Drupal\event_share\Plugin\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;

//Ajax
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Component\Utility\Html;

class ShareForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'resume_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Generate a unique wrapper HTML ID.
    $ajax_wrapper_id = Html::getUniqueId('box-container');
    $form['#prefix'] = '<div id="' . $ajax_wrapper_id . '">';
    $form['#suffix'] = '</div>';

    //Form inputs
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Title message'),
      '#default_value' => "Shere event",
      '#maxlength' => 22,
      '#size' => 64,
      '#weight' => '0',
    ];
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('Friend name'),
      //'#default_value' => $this->configuration['name'],
      '#maxlength' => 22,
      '#size' => 64,
      '#weight' => '0',
      '#required' => TRUE,
    ];
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#description' => $this->t('Friend email'),
      //'#default_value' => $this->configuration['email'],
      '#weight' => '0',
      '#required' => TRUE,
    ];

    /**
     * Normal submit
     *
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#button_type' => 'primary',
    );
     */

    /**
     * Ajax submit
     */
    $form['actions'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => '::setMessage',
      ],
    ];

    return $form;
  }

  /**
   *  Form submit
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $nid = \Drupal::routeMatch()->getParameter('node');

    $event_title    = $nid->title->value;
    $event_date     = date('d.m.Y H:i:s', $nid->get('field_start_end_date')->value);
    $event_location = $nid->get('field_organizer_title')->getValue()[0]['target_id'];
    $event_link     = $nid->toUrl()->toString();

    /**
     * Event location
     */
    $location       = \Drupal::entityTypeManager()->getStorage('node')->load($event_location);
    $location_title = $location->title->value;

    /**
     * Mailer plugin
     */
    $mailManager  = \Drupal::service('plugin.manager.mail');
    $key          = 'send_mail_009';
    $to           = $form_state->getValue('email');
    $langcode     = 'en';
    $send         = true;

    $params['event_title']     = $event_title;
    $params['event_date']      = $event_date;
    $params['event_location']  = $location_title;
    $params['event_link']      = $event_link;
    $params['friend_name']     = $form_state->getValue('name');

    //Send mail
    $resultMail = $mailManager->mail('event_share', $key, $to, $langcode, $params, $send);

    /*if($resultMail['result']!== true){
      drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
    } else {
      drupal_set_message(t('Your message has been sent.'));
    }*/

    /**
     * Include add. vaeriables before header is send (ajax)
     */
    if($resultMail['result']!== true){
      $form_state->set('Submit_success', false);
    }else $form_state->set('Submit_success', true);

    $form_state->setRebuild();

  }

  /**
   * Email validation
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $email = $form_state->getValue('email');
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
      //$form_state->setErrorByName('email', $this->t('Enter a valid email address'));
      //Ajax
      $form_state->set('error_desc', ('Enter a valid email address'));
      $form_state->setRebuild();
    }
  }

  /**
   * Ajax
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public function setMessage(array $form, FormStateInterface $form_state) {
    $success = $form_state->get('Submit_success');
    $message = ($success ? t('Message has been sent') : t('Message has not been sent'));
    $error   = (!is_null($form_state->get('error_desc')) ? '<br><p style="color: red; font-size: 22px;">Error desc: '.$form_state->get('error_desc').'!</p>' : '');
    $response = new AjaxResponse();
    $response->addCommand(
      new HtmlCommand(
        '#box-container',
        '<p class="my_top_message">' .$message.' '.$error.'</p>'
      )
    );

    return $response;
  }


}//en of class