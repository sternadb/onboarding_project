<?php

namespace Drupal\company_email_validation\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks for email addresse.
 *
 * @Constraint(
 *   id = "emailCustomValidation",
 *   label = @Translation("Email no reply", context = "Validation"),
 *   type = "entity:node"
 * )
 */
class Email_no_replyConstraint extends Constraint {

  // The message
  public $notValid = '%value is not valid email, please enter new one!';

}