<?php

namespace Drupal\favorites_count_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides a 'FavoriteBlock' block.
 *
 * @Block(
 *  id = "favorite_block",
 *  admin_label = @Translation("Favorite block"),
 * )
 */
class FavoriteBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $loggedUser   = \Drupal::currentUser();

    /**
     * Use flag services
     */
    $flag         = \Drupal::service('flag')->getFlagById('event_bookmark');
    $flagcount    = \Drupal::service('flag.count')->getUserFlagFlaggingCount($flag, $loggedUser);

    //Create link
    $url_add  = Url::fromUri('internal:/my-favorite-events');
    $link     = Link::fromTextAndUrl(t('Count favorite events @count', ['@count'=> $flagcount]), $url_add)->toString();

    //Time
    $time = (int) microtime(true);

    $build['favorite_block']['#markup'] = t('<p id="counterFavorites">@link</p><p>@time</p>',['@link' =>$link, '@time' => $time]);

    return $build;
  }


  /**
   * Cache max age
   * @return int
   *

  public function getCacheMaxAge() {
    return 8000;
  }*/

  /**
   * Cache Contexts
   * @return array|string[]
   */
  public function getCacheContexts() {
    return ['user'];
  }

  /**
   * Cache Tags
   * @return array|string[]
   */
  public function getCacheTags() {
    return ['favoriteItems'];
  }

}
