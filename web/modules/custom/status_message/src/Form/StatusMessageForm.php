<?php

namespace Drupal\status_message\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class StatusMessageForm.
 */
class StatusMessageForm extends ConfigFormBase {

  protected function getEditableConfigNames() {
    return [
      'status_message.adminsettings',
    ];
}

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'status_message_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('status_message.adminsettings');

    $form['status_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site message'),
      '#description' => $this->t('Message that will be displayed..'),
      '#default_value' => $config->get('status_message'),
    ];

    $form['status_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select message type'),
      '#description' => $this->t('Select type of the status'),
      '#default_value' => $config->get('status_type'),
      '#required' => true,
      '#options' => [
        'status-message-info' => t('Info'),
        'status-message-warning' => t('Warning'),
        'status-message-error' => t('Error'),
        'status-message-success' => t('Success'),
      ]
    ];

    $form['status_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Enabled, for show on top of the page!'),
      '#default_value' => $config->get('status_enabled'),
    ];

    $form['status_admin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Admin enable'),
      '#description' => $this->t('Enabled, for show on admin console!'),
      '#default_value' => $config->get('status_admin'),
    ];


    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('status_message.adminsettings')
      ->set('status_message', $form_state->getValue('status_message'))
      ->set('status_enabled', $form_state->getValue('status_enabled'))
      ->set('status_type', $form_state->getValue('status_type'))
      ->set('status_debug', $form_state->getValue('status_debug'))
      ->set('status_admin', $form_state->getValue('status_admin'))
      ->save();

  }

}
