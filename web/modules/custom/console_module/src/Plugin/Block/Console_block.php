<?php

namespace Drupal\console_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Console_block' block.
 *
 * @Block(
 *  id = "console_block",
 *  admin_label = @Translation("Console_block"),
 * )
 */
class Console_block extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
      $build['console_block']['#Title'] = 'Block';
    $build['console_block']['#markup'] = 'Implement Console_block. Generated';

    return $build;
  }

}
