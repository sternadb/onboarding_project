<?php

namespace Drupal\company_email_validation\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class Email_no_replyConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    foreach ($items as $item) {

      if(self::startsWith($item->value, 'no-reply')){
        $this->context->addViolation($constraint->notValid, ['%value' => $item->value]);
      }
      }

    }

  /**
   * @param $haystack
   * @param $needle
   *
   * @return bool
   **/
  public function startsWith($string, $needle) {
    return $needle === '' || strpos($string, $needle) !== false;
  }

}