<?php

namespace Drupal\event_timer;

use Carbon\Carbon;

/**
 * Class DefaultService.
 */
class DefaultService {

  public $pastEvent = "The event has ended";
  public $progressEvent = "This event is happening today";
  public $futureEvent = " left to event start";

  /**
   * Date and time converter
   * @param $eventDate
   *
   * @return \Carbon\Carbon
   */
  public function date_and_time_converter($eventDate_start, $eventDate_end) {

    $Date_start_all   = Carbon::createFromFormat("Y-m-d H:i:s",$eventDate_start, "Europe/Ljubljana");

    $Date_end_all     = Carbon::createFromFormat("Y-m-d H:i:s",$eventDate_end, "Europe/Ljubljana");

    $Now          = Carbon::now("Europe/Ljubljana")->format('Y-m-d');
    $Now_all      = Carbon::now("Europe/Ljubljana")->format('Y-m-d H:i:s');

    $Date_start = $Date_start_all->format('Y-m-d');
    $Date_end   = $Date_end_all->format('Y-m-d');

    $Now        = Carbon::parse($Now);
    $Date_start = Carbon::parse($Date_start);
    $Date_end   = Carbon::parse($Date_end);

    //In progress
    if($Now->gte($Date_start) && $Date_end->gt($Now)){
      return $this->progressEvent;
    }else{
      //Pass
      if($Now->gt($Date_end)) return $this->pastEvent;
      //future
      if($Now->lt($Date_start)){
        $diff = $Now->diffInDays($Date_start);
        $diffH  = ($Now->addDays($diff))->diffInHours($Date_start_all);
        return $diff.' days and '.$diffH.'h'.$this->futureEvent;

      }
    }

    //return $carbonDate;
  }


}
