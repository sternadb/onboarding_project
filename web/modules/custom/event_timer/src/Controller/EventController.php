<?php

namespace Drupal\event_timer\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class EventController.
 */
class EventController extends ControllerBase {

  /**
   * Strat.
   *
   * @return string
   *   Return Hello string.
   */
  public function start() {
    return [
      '#title' => 'Event timer',
      '#markup' => $this->t('Date and time info')
    ];
  }

}
