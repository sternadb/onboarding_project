<?php

namespace Drupal\event_timer\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'EventTimerBlock' block.
 *
 * @Block(
 *  id = "event_timer_block",
 *  admin_label = @Translation("Event timer block"),
 * )
 */
class EventTimerBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $node = \Drupal::routeMatch()->getParameter('node');
    $date_time = $node->get('field_start_end_date')->getValue();
    $date_time_start = date('Y-m-d H:i:s', $date_time[0]['value']);
    $date_time_end  = date('Y-m-d H:i:s', $date_time[1]['value']);

    //$eventTitle = $node->title->value;

    $service = \Drupal::service("event_timer.date")->date_and_time_converter($date_time_start, $date_time_end);


    //$build['#theme'] = 'eventTimer';
    //$build['#attached']['library'][] = 'event_timer/timer.global';
    $build['#markup'] = 'Event status<br><br>'.$service;
    $build['#cache']['max-age'] = 0;

    return $build;
  }


}
