<?php

namespace Drupal\fields_formatters\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Plugin implementation of the 'title_and_icon_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "title_and_icon_field_formatter",
 *   label = @Translation("Title and icon field formatter"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class TitleAndIconFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {

        $term = Term::load($item->target_id);

        $title = $term->getName();

        $image = $term->get('field_icon');

        //Load default image
        if($image->entity == null){

          $image_default    = $image->getSetting('default_image')['uuid'];

          $defaultImageFile = \Drupal::service('entity.repository')->loadEntityByUuid('file', $image_default);

          $image_url = $defaultImageFile->getFileUri();

        }else $image_url = $image->entity->uri->value;

        //Create url
        $image_url = file_create_url($image_url);

        $elements[$delta] = ['#markup' => ' <img src="'.$image_url.'" width="50" height="50">'.$title];


    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
