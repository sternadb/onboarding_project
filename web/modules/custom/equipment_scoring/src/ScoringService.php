<?php

namespace Drupal\equipment_scoring;

/**
 * Class ScoringService.
 */
class ScoringService {


  private $term           = 'equipment_';
  private $equipmentsArr  = array();

  /**
   * Constructs a new ScoringService object.
   */
  public function __construct() {
    self::getAllEquipments();
  }


  public function getAllEquipments(){
      $dbTerms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($this->term);

      foreach ($dbTerms as $term){
        $equip =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($term->tid);

        $equip_id = $equip->id();
        $equip_name = $equip->getName();
        $equip_field_rationg = $equip->get('field_rating')->value;

        $equip_arr = array('id' => $equip_id, 'name' => $equip_name, 'rating' => $equip_field_rationg);

        //Push to array
        array_push($this->equipmentsArr, $equip_arr);

      }

  }


  /**
   * Calculate location rating
   * @param $node_equipments
   */
  public function calculateScoringStars($node_equipments){
    $rating    = 0;
    $count_equ = 0;

    $node_equipments = $node_equipments;
    $all_equipments = $this->equipmentsArr;

    foreach ($node_equipments as $node_equ){
      $node_equ_id = $node_equ->id();
      $node_equ_rating = $node_equ->get('field_rating')->value;

      $rating+= $node_equ_rating;
      $count_equ++;
    }

    $ratingLocation = $rating/$count_equ;

    return (integer)$ratingLocation;

  }

}
