<?php

namespace Drupal\console_module\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class ConsoleController.
 */
class ConsoleController extends ControllerBase {

    public $name = "Test Name";
    public $years = 22;

  /**
   * Action_method.
   *
   * @return string
   *   Return Hello string.
   */
  public function Action_method() {
      self::SetName();
      $name = $this->name;
      $years = $this->years;

    return [
      '#type' => 'markup',
      '#markup' => $this->t('Implement method: Action_method| <br> <p style="color:red;">NAME:'.$name.' YEARS:'.$years)
    ];
  }

  protected function SetName(){
        $this->name = "Blaž";
        $this->years = 29;
  }

}
