<?php

namespace Drupal\equipment_scoring\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ScoringBlock' block.
 *
 * @Block(
 *  id = "scoring_block",
 *  admin_label = @Translation("Scoring block"),
 * )
 */
class ScoringBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    /**
     * Variables
     */
    $node             = \Drupal::routeMatch()->getParameter('node');
    $nodeType         = $node->getType();
    $equipmentsFields = null;
    $location_rating  = 0;

    //Check for equipments fields
    if($nodeType == 'location'){
      $equipmentsFields = $node->get('field_equipment');
      $equipmentsFields = $equipmentsFields->referencedEntities();
    }

    if(!empty($equipmentsFields)){
      //Initialization services
      $service = \Drupal::service("scoringservice");

      $location_rating = \Drupal::service("scoringservice")->calculateScoringStars($equipmentsFields);
    }

    $build = [
        '#theme' => 'equipment_scoring',
        '#ranking_result' => $location_rating,
        '#attached' => [
          'library' => ['equipment_scoring/equipment_scoring'],
        ]

    ];

    return $build;
  }

  public function getCacheMaxAge() {
    return 0;
  }


}
